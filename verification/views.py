from rest_framework.views import APIView
from verification.models import User, Registration
from django.db.utils import IntegrityError
from rest_framework.response import Response
from rest_framework import serializers, status
from verification.utils import send_code, make_iota_transaction
import requests
from django.conf import settings

class RegistrationSerializer(serializers.ModelSerializer):
    emailId = serializers.EmailField(write_only=True)
    class Meta:
        model = Registration
        fields = ('ePublicKey','ePrivateKey','sPublicKey','sPrivateKey', 'emailId')

    def create(self, validated_data):
    # try:
        emailId = validated_data.pop("emailId")
        user = User.objects.create_user(emailId, email=emailId)
        request = self.context.get('request')
        key = Registration.objects.create(user=user, **validated_data)
        if settings.DEVELOPMENT:
            user.confirm_email(user.confirmation_key)
            make_iota_transaction(user.registration.ePublicKey, user.registration.sPublicKey, user.email)
        else:
            send_code(email, user.confirmation_key, request)
        return key

# Create your views here.
class SendEmailView(APIView):
    """
    POST: EmailID, PublicKey, PrivateKey, Vector fields required
    """
    def post(self, request):
        s = RegistrationSerializer(data=request.data, context={"request":request})
        if s.is_valid():
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)
    
class RegistrationDetailView(APIView):
    """
    GET: The /email@example.com will retrive the public key and encrypted private key
    """
    def get(self, request, email):
        try:
            data = Registration.objects.get(user__username=email)
            return Response(RegistrationSerializer(data).data)
        except Registration.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
         

class VerifyEmailView(APIView):
    """
    GET: The code sent to the email
    """
    def get(self, request, code, email):
        try:
            user = User.objects.get(username=email)
        except User.DoesNotExist:
            return Response({"error":"The email id does not exist in the database. Make a post request first."}, status=status.HTTP_400_BAD_REQUEST)
        if user.is_confirmed:
            return Response({"error":"User already confirmed","PublicKey":user.registration.PublicKey}, status=status.HTTP_200_OK)
        else:
            user.confirm_email(code)
            # Send transaction on Tangle
            make_iota_transaction(user.registration.PublicKey, user.email)
            if user.is_confirmed:
                return Response({"success":"Email verification successful", "PublicKey":user.registration.PublicKey}, status=status.HTTP_202_ACCEPTED)
            