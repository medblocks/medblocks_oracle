import requests
from medblocks_oracle.settings import MAILGUN_DOMAIN, MAILGUN_API_KEY
from django.urls import reverse
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from base64 import b64encode
from medblocks_oracle.settings import RSA_KEY
from django.conf import settings
import json
import requests

def send_code(to, code, request):
    url = "{scheme}://{host}{path}".format(
        scheme=request.scheme, 
        host=request.get_host(),
        path=reverse('email-verify', kwargs={'email':to,'code':code})
        )
    return requests.post(
        "https://api.mailgun.net/v3/{}/messages".format(MAILGUN_DOMAIN),
        auth=("api", MAILGUN_API_KEY),
        data={
            "from":"Team MedBlocks <verification@medblocks.org>",
            "to": [to],
            "subject": "MedBlocks Verification",
            "text": "Click on this link to verify: {}".format(url)
        }
    )
def sign_string(string):
    rsa_key = settings.RSA_KEY
    rsa_key = rsa_key.encode().decode('unicode_escape')
    rsa_key = RSA.importKey(rsa_key.encode())
    signer = PKCS1_v1_5.new(rsa_key)
    hash = SHA256.new(string.encode())
    signed = signer.sign(hash)
    signed = b64encode(signed)
    return signed.decode()

def make_iota_transaction(ePublicKey, sPublicKey, emailId):
    data = {
        "ePublicKey":ePublicKey,
        "sPublicKey":sPublicKey,
        "emailId":emailId
        }
    data_string = json.dumps(data)
    message = json.dumps(
            {
                "data": data_string,
                "signature":sign_string(data_string)
            }
        )
    data = {"message": message, "tag": "GFAQBESKMJIPYWPARQBZMROJVPP", "address":emailId}
    r = requests.post("http://localhost:8080/registerUser", json=data)
    print(r)